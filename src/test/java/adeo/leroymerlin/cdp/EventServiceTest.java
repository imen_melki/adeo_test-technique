package adeo.leroymerlin.cdp;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@SpringBootTest
public class EventServiceTest {


    @Mock
    private EventRepository eventRepository;

    @InjectMocks
    private EventService eventService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetFilteredEvents() {
        // Mock data and repository behavior
        List<Event> mockEvents = Arrays.asList(new Event(), new Event());
        when(eventRepository.findAllBy()).thenReturn(mockEvents);

        // Test the getFilteredEvents function
        List<Event> events = eventService.getFilteredEvents("rock");

        assertEquals(2, events.size());

    }



    @Test
    public void testGetFilteredEventsNoResults() {
        // Mock repository behavior
        when(eventRepository.findAllBy()).thenReturn(Collections.emptyList());

        // Test the getFilteredEvents function
        List<Event> events = eventService.getFilteredEvents("");

        assertEquals(0, events.size());
    }
}
