package adeo.leroymerlin.cdp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class EventService {

    private final EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<Event> getEvents() {
        return eventRepository.findAllBy();
    }

    public void delete(Long id) {
        eventRepository.delete(id);
    }

    public List<Event> getFilteredEvents(String query) {
        List<Event> events = eventRepository.findAllBy();

        // Create a pattern to match the given query
        Pattern pattern = Pattern.compile(query);

        // Filter the events list to only include events that have at least one band with a member whose name matches the given pattern
        return events.stream()
                .filter(event -> event.getBands() != null) // Add a null check
                .filter(event -> event.getBands().stream()
                        .anyMatch(band -> band.getMembers().stream()
                                .anyMatch(member -> pattern.matcher(member.getName()).matches())))
                .collect(Collectors.toList());
    }

    public Event updateEvent(Event event) {
        return eventRepository.save(event);
    }
}
